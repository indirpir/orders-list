# ABOUT:
* This is orders list demo project
* Available features: orders list filtering (by status)
* This project had been used as a template for project- https://github.com/gtalarico/django-vue-template 


# INSTALL & RUN:
Prerequizities: `virtualenv`, `yarn`.
```
git clone https://bitbucket.org/indirpir/orders-list.git
cd orders-list

# build frontend
yarn install
yarn build

# run backend
virtualenv -p python3 env
source env/bin/activate
pip install -r requirements.pip
./manage.py runserver

# Go check out http://localhost:8000/
```


# BUILD FRONTEND:
```
yarn install
yarn build
```


# FOR DJANGO ADMIN ACCESS:
* Visit `http://localhost:8000//api/admin` (Login: zz, Password: zz)
