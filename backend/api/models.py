from decimal import Decimal

from django.db import models
from django.db.models import Sum, F, DecimalField
from django_extensions.db.models import TimeStampedModel
from model_utils import Choices


STATUSES = Choices((False, 'IN_PROGRESS', 'In Progress'),
                   (True, 'COMPLETE', 'Complete'))


# ==================== QUERYSETS =============================

class OrderQuerySet(models.QuerySet):
    def preload_totals(self):
        return self.annotate(
            total=Sum(
                F('items__qty') * F('items__product__price'),
                output_field=DecimalField()
            )
        )

# ==================== MODELS ================================


class Product(TimeStampedModel):
    name = models.TextField()
    price = models.DecimalField(
        max_digits=16, decimal_places=2, default=Decimal(0)
    )

    def __str__(self):
        return self.name


class Order(TimeStampedModel):
    status = models.BooleanField(
        choices=STATUSES, default=STATUSES.IN_PROGRESS
    )
    products = models.ManyToManyField(Product, through='OrderItem')

    objects = OrderQuerySet.as_manager()

    def __str__(self):
        return "Order #{}".format(self.pk)


class OrderItem(models.Model):
    order = models.ForeignKey(
        Order, on_delete=models.CASCADE, related_name='items',
    )
    product = models.ForeignKey(
        Product, on_delete=models.CASCADE, related_name='order_items',
    )
    qty = models.PositiveIntegerField()

    def __str__(self):
        return "{} (x{})".format(self.product.name, self.qty)
