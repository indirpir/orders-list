from rest_framework import serializers


class VerboseChoiceField(serializers.ChoiceField):
    def get_model_field(self):
        meta = getattr(self.parent, 'Meta', None)
        meta = meta.model._meta
        return meta.get_field(self.source)

    def to_representation(self, value):
        if self.choices:
            name = dict(self.choices).get(value)
        else:
            field = self.get_model_field()
            name = dict(field.choices).get(value)

        if name:
            return {'id': value, 'value': name}


class MultiSerializerViewSetMixin(object):
    serializer_action_classes = {}

    def get_serializer_class(self):
        """
        Look for serializer class in self.serializer_action_classes, which
        should be a dict mapping action name (key) to serializer class (value),
        i.e.:

        class MyViewSet(MultiSerializerViewSetMixin, ViewSet):
            serializer_class = MyDefaultSerializer
            serializer_action_classes = {
               'list': MyListSerializer,
               'my_action': MyActionSerializer,
            }

            @action
            def my_action:
                ...

        If there's no entry for that action then just fallback to the regular
        get_serializer_class lookup: self.serializer_class, DefaultSerializer.

        Thanks gonz: http://stackoverflow.com/a/22922156/11440

        """
        try:
            return self.serializer_action_classes[self.action]
        except (KeyError, AttributeError):
            return super(
                MultiSerializerViewSetMixin, self,
            ).get_serializer_class()
