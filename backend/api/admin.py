from django.contrib import admin

from .models import Product, OrderItem, Order


class ProductAdmin(admin.ModelAdmin):
    model = Product
    fields = ('name', 'price', )
    list_display = ('pk', 'name', 'price', )


class OrderItemInline(admin.TabularInline):
    # fields = ('product', '')
    model = OrderItem


class OrderAdmin(admin.ModelAdmin):
    model = Order
    fields = ('status', )
    inlines = (OrderItemInline, )
    list_display = ('pk', 'created', 'status', )


admin.site.register(Product, ProductAdmin)
admin.site.register(Order, OrderAdmin)
