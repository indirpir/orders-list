from rest_framework import serializers
from .models import Order, Product, OrderItem
from .common import VerboseChoiceField


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('pk', 'name', 'price')


class OrderItemSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = OrderItem
        fields = ('pk', 'product', 'qty')


class OrderDetailSerializer(serializers.HyperlinkedModelSerializer):
    name = serializers.ReadOnlyField(source='__str__')
    items = OrderItemSerializer(many=True)
    total = serializers.ReadOnlyField()
    serializer_choice_field = VerboseChoiceField

    class Meta:
        model = Order
        fields = ('pk', 'status', 'items', 'name', 'created', 'total', )


class OrderListSerializer(serializers.ModelSerializer):
    name = serializers.ReadOnlyField(source='__str__')
    serializer_choice_field = VerboseChoiceField

    class Meta:
        model = Order
        fields = ('pk', 'status', 'name', 'created', )
