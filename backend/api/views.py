from django.views.generic import TemplateView
from django.views.decorators.cache import never_cache
from django_filters import rest_framework as filters


from rest_framework import viewsets

from .models import Order
from .serializers import OrderDetailSerializer, OrderListSerializer
from .common import MultiSerializerViewSetMixin


index_view = never_cache(TemplateView.as_view(template_name='index.html'))


class OrdersFilter(filters.FilterSet):
    status = filters.BooleanFilter()

    class Meta:
        model = Order
        fields = ('status', )


class OrdersViewSet(
        MultiSerializerViewSetMixin,
        viewsets.ModelViewSet,
        ):
    """
    API endpoint that allows messages to be viewed or edited.
    """
    queryset = Order.objects.preload_totals().all()
    serializer_class = OrderDetailSerializer
    serializer_action_classes = {
        'list': OrderListSerializer,
    }
    filterset_class = OrdersFilter
