import axios from 'axios'
import Vapi from 'vuex-rest-api'
import Cookies from 'js-cookie'
import queryString from 'query-string'

export default new Vapi({
    axios: axios.create({
      baseURL: '/api',
      timeout: 5000,
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': Cookies.get('csrftoken')
      }
    }),
    state: {
      ordersParams: {},
      selectedOrder: undefined,
      orders: [],
    }
  })
  .get({
    action: "getOrder",
    property: "selectedOrder",
    path: ({ pk }) => `/orders/${pk}`,
  })
  .get({
    action: "listOrders",
    property: "orders",
    path: params => (
      `/orders?${queryString.stringify(params)}`
    ),
    onSuccess(state, payload, axios, { params }) {
      state.orders = payload.data;
      state.ordersParams = params;
      console.log('[onSuccess] params=', params);
    },
  })
  .post({
    action: "updateOrder",
    property: "order",
    path: ({ id }) => `/orders/${id}`
  });
