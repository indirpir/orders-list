import Vue from 'vue'
import Vuex from 'vuex'
import ordersApi from '@/services/api'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    orders: Object.assign(
      ordersApi.getStore(),
      { namespaced: true },
    ),
  }
})
