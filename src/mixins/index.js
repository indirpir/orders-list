import moment from 'moment'

export default {
  methods: {
    formatDatetime: str => moment(str).format('DD.MM.YYYY hh:mm'),
  },
};
